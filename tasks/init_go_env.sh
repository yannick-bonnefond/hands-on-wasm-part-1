#!/bin/bash

directory=$1
environment=$2

cat > .vscode/settings.json <<- EOM
{
  "workbench.iconTheme": "material-icon-theme",
   "workbench.colorTheme": "Monokai",
  "go.toolsEnvVars": {
    "GOOS": "js",
    "GOARCH": "wasm",
  },
  "go.goroot": "/home/${directory}/.gvm/gos/go1.17.5",
  "go.gopath": "/home/${directory}/.gvm/pkgsets/go1.17.5/global",
  "gopls": {
    "experimentalWorkspaceModule": true,
  },
  "terminal.integrated.defaultProfile.linux": "bash",
  "task.autoDetect": "on",
  "task.problemMatchers.neverPrompt": true
}
EOM
echo "[${environment}] Golang environment ✅"
